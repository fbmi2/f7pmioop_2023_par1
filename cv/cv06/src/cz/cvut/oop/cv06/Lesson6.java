package cz.cvut.oop.cv06;

public class Lesson6 {
    /*

    Základní koncepty:
    Třída a Objekt:
    Třída je šablona, podle které jsou vytvářeny objekty.
    Objekty jsou instance tříd a obsahují vlastnosti (atributy) a chování (metody).
    Dědičnost:
    Dědičnost umožňuje vytvářet nové třídy na základě existujících tříd.
    Nová třída (podtřída nebo potomek) může zdědit vlastnosti a metody ze své nadřazené třídy (nadtřídy nebo rodiče).

    Klíčová slova a koncepty:
    extends: Používá se k deklaraci dědičnosti. Třída potomka rozšiřuje třídu rodiče.
    super: Používá se k odkazování na konstruktor rodičovské třídy a k přístupu k prvkům nadřazené třídy.

    Veřejné (public), chráněné (protected) a privátní (private) atributy rodičovské třídy jsou zděděny v potomku, ale přístup k nim závisí na jejich přístupových právech.
    Java nepodporuje vícenásobnou dědičnost, ale můžete vytváře hierarchii tříd
     */

    public static void main(String[] args) {
        Circle circle1 = new Circle("red", 26);

        System.out.println(circle1);
        System.out.println(circle1.getArea());
        System.out.println(circle1.getColor());

        Rectangle rectangle1 = new Rectangle("blue", 10, 5);
        System.out.println(rectangle1);
        System.out.println(rectangle1.getArea());
        System.out.println(rectangle1.getColor());

        Square square1 = new Square("yellow", 15);
        System.out.println(square1);
        System.out.println(square1.getArea());
        System.out.println(square1.getColor());

    }

}

class Shape {
    private String color;

    public Shape(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }
}

class Circle extends Shape {

    private double radius;

    public Circle(String color, double radius) {
        super(color); // Volání konstruktoru nadřazené třídy
        this.radius = radius;
    }

    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public String toString() {
        return "Kruh s obsahem " + getArea() + " a barvou: " + super.getColor();
    }


}

class Rectangle extends Shape {
    private double width;
    private double height;

    public Rectangle(String color, double width, double height) {
        super(color); // Volání konstruktoru nadřazené třídy
        this.width = width;
        this.height = height;
    }

    public double getArea() {
        return width * height;
    }

    @Override
    public String toString() {
        return "Area of rectange: " + this.getArea() + " with color:" + super.getColor();
    }

}

class Square extends Rectangle {

    public Square(String color, double width) {
        super(color, width, width); // Volání konstruktoru nadřazené třídy
    }

}

