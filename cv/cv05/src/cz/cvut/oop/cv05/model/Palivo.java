package cz.cvut.oop.cv05.model;

public enum Palivo {

    BENZIN,
    DIESEL,
    ELECTRO,
    HYDROGEN,
    VODKA
}
