package cz.cvut.oop.cv05.model;

public class Motor {

    private Palivo palivo;
    private double objem;

    public Motor(Palivo palivo, double objem) {
        this.palivo = palivo;
        this.objem = objem;
    }

    public Palivo getPalivo() {
        return palivo;
    }

    public void setPalivo(Palivo palivo) {
        this.palivo = palivo;
    }

    public double getObjem() {
        return objem;
    }

    public void setObjem(double objem) {
        this.objem = objem;
    }
}
