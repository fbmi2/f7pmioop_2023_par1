package cz.cvut.oop.cv05.model;

public class Auto {

    private String znacka;
    private String barva;
    private int rokVyroby;

    private Motor motor;

    public Auto(String znacka, String barva, int rokVyroby, Motor motor) {
        this.znacka = znacka;
        this.barva = barva;
        this.rokVyroby = rokVyroby;
        this.motor = motor;
    }

    public String getZnacka() {
        return znacka;
    }

    public void setZnacka(String znacka) {
        this.znacka = znacka;
    }

    public String getBarva() {
        return barva;
    }

    public void setBarva(String barva) {
        this.barva = barva;
    }

    public int getRokVyroby() {
        return rokVyroby;
    }

    public void setRokVyroby(int rokVyroby) {
        this.rokVyroby = rokVyroby;
    }

    public void vytiskniAuto() {
        System.out.println(znacka + "," + this.rokVyroby + "," + this.barva + "," + this.motor.getPalivo()
        + "," + this.motor.getObjem());
    }

    @Override
    public String toString() {
        return znacka + "," + this.rokVyroby + "," + this.barva + "," + this.motor.getPalivo()
                + "," + this.motor.getObjem();
    }



}
