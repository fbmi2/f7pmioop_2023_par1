package cz.cvut.oop.cv05;

import cz.cvut.oop.cv05.model.Palivo;
import cz.cvut.oop.cv05.model.Auto;
import cz.cvut.oop.cv05.model.Motor;

public class Cviceni5 {

    public static void main(String[] args) {

        Auto auto1 = new Auto("Toyota", "černá", 2023, new Motor(Palivo.BENZIN, 1800));
        Auto auto2 = new Auto("Ford", "černá", 2023, new Motor(Palivo.BENZIN, 1800));
        Auto auto3 = new Auto("Mazda", "černá", 2023, new Motor(Palivo.BENZIN, 1800));
        Auto auto4 = new Auto("Škoda", "černá", 2023, new Motor(Palivo.BENZIN, 1800));

        auto2 = auto4;
        auto3 = auto1;
        auto2 = auto3;
        auto1 = auto4;
        auto3 = auto1;

        System.out.println(auto1.equals(auto4));
        System.out.println(auto1);
        System.out.println(auto1.hashCode());

        //System.out.println(auto1);
        //auto1.vytiskniAuto();

        //System.out.println(2 == 2);
        //System.out.println(2 > 2);

        //System.out.println("ABC" == "ABC");
        //System.out.println(new String("ABC") == new String("ABC"));

        //System.out.println(auto1 == auto2);
        //System.out.println(auto1.equals(auto2));

    }

    public Auto createSUV(String znacka, String barva, int rokVyroby) {
        Auto suv = new Auto(znacka, barva, rokVyroby, new Motor(Palivo.BENZIN, 3.0));
        return suv;
    }



}
