package cz.cvut.oop.cv05ex;

import cz.cvut.oop.cv05ex.model.Smrtelnost;
import cz.cvut.oop.cv05ex.model.Nemoc;
import cz.cvut.oop.cv05ex.model.Pacient;

public class Application {

    public static void main(String[] args) {



        Nemoc angina1 = new Nemoc("Angina", Smrtelnost.STREDNI);
        Nemoc angina2 = new Nemoc("Angina", Smrtelnost.STREDNI);
        Nemoc specialniAngina = new Nemoc("Angina", Smrtelnost.NEJVYSSI);
        Pacient pacient1 = new Pacient("Bohuslav", 32);

        System.out.println(angina1.equals(specialniAngina));

        pacient1.pridejNemoc(angina1);
        pacient1.pridejNemoc(angina2);
        pacient1.pridejNemoc(specialniAngina);

        pacient1.vypisNemoci();



    }
}
