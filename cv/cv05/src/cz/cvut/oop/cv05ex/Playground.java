package cz.cvut.oop.cv05ex;

public class Playground {

    public static void main(String[] args) {

        // jaký je rozdíl mezi metodou equals() a mezi operátorem ==

        String prvniString = new String("Příklad");
        String druhyString = new String("Příklad");

        if (prvniString.equals(druhyString)) {
            System.out.println("Obsah řetězců je stejný.");
        } else {
            System.out.println("Obsah řetězců není stejný.");
        }
        System.out.println(prvniString.equals(druhyString));


        if (prvniString == druhyString) {
            System.out.println("Reference na stejnou instanci v paměti.");
        } else {
            System.out.println("Reference nejsou stejné.");
        }
        System.out.println(prvniString == druhyString);


    }

}
