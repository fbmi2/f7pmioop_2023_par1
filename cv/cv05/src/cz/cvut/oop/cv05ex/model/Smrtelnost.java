package cz.cvut.oop.cv05ex.model;

public enum Smrtelnost {
    VELMI_NIZKA,
    NIZKA,
    STREDNI,
    VYSSI,
    NEJVYSSI
}
