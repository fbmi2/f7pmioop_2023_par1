package cz.cvut.oop.cv05ex.model;

import java.util.ArrayList;

public class Pacient {

    private String jmeno;
    private int vek;

    private ArrayList<Nemoc> nemoci;

    public Pacient(String jmeno, int vek) {
        this.jmeno = jmeno;
        this.vek = vek;
        this.nemoci = new ArrayList<Nemoc>();
    }


    public void pridejNemoc(Nemoc novaNemoc) {
        boolean jizProdelana = false;
        for (Nemoc prodelanaNemoc : nemoci) {
            if (novaNemoc.equals(prodelanaNemoc)) {
                jizProdelana = true;
                break;
            }
        }
        if (jizProdelana) {
            System.out.println("Pacient již prodělal tuto nemoc.");
        } else {
            this.nemoci.add(novaNemoc);
        }
    }

    public void vypisNemoci() {
        System.out.println("Pacientovi nemoci:");
        for (Nemoc nemoc : nemoci) {
            System.out.println(nemoc);
        }
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public int getVek() {
        return vek;
    }

    public void setVek(int vek) {
        this.vek = vek;
    }
}
