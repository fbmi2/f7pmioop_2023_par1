package cz.cvut.oop.cv05ex.model;

public class Nemoc {

    private String nazevNemoci;
    private Smrtelnost smrtelnost;

    public Nemoc(String nazevNemoci, Smrtelnost smrtelnost) {
        this.nazevNemoci = nazevNemoci;
        this.smrtelnost = smrtelnost;
    }

    public String getNazevNemoci() {
        return nazevNemoci;
    }

    public void setNazevNemoci(String nazevNemoci) {
        this.nazevNemoci = nazevNemoci;
    }

    public Smrtelnost getSmrtelnost() {
        return smrtelnost;
    }

    public void setSmrtelnost(Smrtelnost smrtelnost) {
        this.smrtelnost = smrtelnost;
    }

    @Override
    public String toString() {
        return nazevNemoci + " : " + smrtelnost;
    }

    @Override
    public boolean equals(Object obj) {
        // pokud ukazuji na stejne misto v pameti, pak jsou stejne
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Nemoc nemocZeVstupu = (Nemoc) obj;
        return this.getNazevNemoci().equals(nemocZeVstupu.getNazevNemoci()) && this.getSmrtelnost().equals(nemocZeVstupu.getSmrtelnost());
    }

}
