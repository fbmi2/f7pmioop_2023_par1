package cvut.oop.cv04;

import cvut.oop.animals.Dog;

public class Person {

    private String name;
    private int age;
    private Dog dog;


    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person(String name, int age, Dog dog) {
        this.name = name;
        this.age = age;
        this.dog = dog;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setAge() {
        this.age = this.age + 1;
    }

    public void tellNameAndAge() {
        System.out.println("Name: " + this.name + ", age:" + this.age);
    }

    public String getName() {
        return this.name;
    }

    public void tellName() {
        System.out.println("Moje jméno je" + this.name);
    }

}
