package cvut.oop.cv04;

import cvut.oop.animals.Dog;

import java.util.ArrayList;

public class Cviceni4 {

    public static void main(String[] args) {

        // lets create an object
        ArrayList<Integer> integers = new ArrayList<>();

        integers.add(12);
        integers.add(25);
        integers.add(34);
        integers.add(100);

        Person person1 = new Person("Bohuslav", 32);


        //System.out.println(integers.size());
        //System.out.println(person1);
        person1.tellNameAndAge();

        //System.out.println(person1Name);
        //System.out.println(person1.giveName());


        Person person2 = new Person("Anna", 31);
        Person person3 = new Person("Božena", 102);

        //person2.tellNameAndAge();
        //person3.tellNameAndAge();

        person1.tellName();

        Person person4 = new Person("Hana", 25);
        Person person5 = new Person("Tomáš", 25, new Dog());

    }

}
