package cvut.fbmi.opp.cv08;

import cvut.fbmi.opp.cv08.model.*;

import java.util.HashMap;

public class Application {
    public static void main(String[] args) {

        HashMap<ServiceTaskName, ServiceTask> serviceTasks = new HashMap<>();

        serviceTasks.put(ServiceTaskName.OIL_CHANGE,new ServiceTask(ServiceTaskName.OIL_CHANGE,
                30,100));
        serviceTasks.put(ServiceTaskName.BRAKES_CHECK_AND_REPLACE,new ServiceTask(ServiceTaskName.BRAKES_CHECK_AND_REPLACE,
                120,250));
        serviceTasks.put(ServiceTaskName.GEOMETRY,new ServiceTask(ServiceTaskName.GEOMETRY,
                60,70));
        serviceTasks.put(ServiceTaskName.TECHNICAL_INSPECTION,new ServiceTask(ServiceTaskName.TECHNICAL_INSPECTION,
                60,100));
        serviceTasks.put(ServiceTaskName.TIRES_REPLACEMENT,new ServiceTask(ServiceTaskName.TIRES_REPLACEMENT,
                30,50));


        HashMap<Integer, Technician> technicians = new HashMap<>();
        technicians.put(1,new Technician("Rudolf Jelínek", "Lihovarnická 12, 14500, Praha 4",
                "jelinek@autoservissmrt.cz","723456789"));
        technicians.put(2,new Technician("Johny Walker", "Automobilová 22, 14500, Praha 4",
                "walker@autoservissmrt.cz","723987654"));


        Customer customer1 = new Customer("Štěpán Chudák", "Ukradená 30, 12345, Liberec",
                "chudak@nomoney.com", "733444555");

        Service serviceChudaka = new Service(technicians.get(1),new Car("Ford","4AH1234",1998),
                customer1,serviceTasks.get(ServiceTaskName.BRAKES_CHECK_AND_REPLACE));

        Technician jelinek = technicians.get(1);

        jelinek.smokeCigarete(serviceChudaka);
        System.out.println(serviceChudaka);
        jelinek.doNothing(serviceChudaka);
        System.out.println(serviceChudaka);
        jelinek.smokeCigarete(serviceChudaka);
        System.out.println(serviceChudaka);
        jelinek.fixTheCar(serviceChudaka);
        System.out.println(serviceChudaka);




    }
}
