package cvut.fbmi.opp.cv08.model;

public class ServiceTask {

    private ServiceTaskName taskName;
    private int typicalDurationInMinutes;
    private int typicalPriceInEuros;

    public ServiceTask(ServiceTaskName taskName, int typicalDurationInMinutes, int typicalPriceInEuros) {
        this.taskName = taskName;
        this.typicalDurationInMinutes = typicalDurationInMinutes;
        this.typicalPriceInEuros = typicalPriceInEuros;
    }

    public ServiceTaskName getTaskName() {
        return taskName;
    }

    public int getTypicalDurationInMinutes() {
        return typicalDurationInMinutes;
    }

    public int getTypicalPriceInEuros() {
        return typicalPriceInEuros;
    }
}
