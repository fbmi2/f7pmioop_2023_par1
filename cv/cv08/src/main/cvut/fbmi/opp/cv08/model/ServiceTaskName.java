package cvut.fbmi.opp.cv08.model;

public enum ServiceTaskName {

    OIL_CHANGE,
    TIRES_REPLACEMENT,
    GEOMETRY,
    BRAKES_CHECK_AND_REPLACE,
    TECHNICAL_INSPECTION

}
