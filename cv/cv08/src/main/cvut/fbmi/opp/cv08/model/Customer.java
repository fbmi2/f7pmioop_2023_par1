package cvut.fbmi.opp.cv08.model;

public class Customer extends Person{
    public Customer(String name, String adress, String email, String phoneNumber) {
        super(name, adress, email, phoneNumber);
    }
}
