package cvut.fbmi.opp.cv08.model;

public class Service {

    private int price;
    private int duration;
    private boolean isFinished;
    private Technician technician;
    private Car car;
    private Customer customer;
    private ServiceTask serviceTask;

    public Service(Technician technician, Car car, Customer customer, ServiceTask serviceTask) {
        this.price = serviceTask.getTypicalPriceInEuros();
        this.duration = serviceTask.getTypicalDurationInMinutes();
        this.isFinished = Boolean.FALSE;
        this.technician = technician;
        this.car = car;
        this.customer = customer;
        this.serviceTask = serviceTask;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setFinished(boolean finished) {
        isFinished = finished;
    }

    public int getPrice() {
        return price;
    }

    public int getDuration() {
        return duration;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public Technician getTechnician() {
        return technician;
    }

    public Car getCar() {
        return car;
    }

    public Customer getCustomer() {
        return customer;
    }

    public ServiceTask getServiceTask() {
        return serviceTask;
    }

    @Override
    public String toString() {
        if (isFinished()) {
            return "Servis auta " + car.getRegistrationNumber() + " byl dokončen, cena je: " + getPrice() + " trvalo to " +
                    getDuration() + " minut";
        } else {
            return "Servis auta " + car.getRegistrationNumber() + " stále pokračuje, technik provádí důležité úkony. " +
                    "Aktuální cena je: " + getPrice();
        }
    }
}
