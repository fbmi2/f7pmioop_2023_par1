package cvut.fbmi.opp.cv08.model;

public class Car {

    private String brand;
    private String registrationNumber;
    private int year;

    public Car(String brand, String registrationNumber, int year) {
        this.brand = brand;
        this.registrationNumber = registrationNumber;
        this.year = year;
    }

    public String getBrand() {
        return brand;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public int getYear() {
        return year;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }
}
