package cvut.fbmi.opp.cv08.model;

public class Technician extends Person implements TechinicalJob {
    public Technician(String name, String adress, String email, String phoneNumber) {
        super(name, adress, email, phoneNumber);
    }

    @Override
    public void smokeCigarete(Service service) {
        service.setDuration(service.getDuration()+15);
        service.setPrice(service.getPrice()+10);
    }

    @Override
    public void fixTheCar(Service service) {
        service.setFinished(Boolean.TRUE);
    }

    @Override
    public void doNothing(Service service) {
        service.setDuration(service.getDuration()+20);
        service.setPrice(service.getPrice()+25);
    }
}
