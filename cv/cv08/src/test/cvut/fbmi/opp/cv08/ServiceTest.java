package cvut.fbmi.opp.cv08;

import cvut.fbmi.opp.cv08.model.*;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


public class ServiceTest {

    @Test
    public void testServiceisDone() {

        ServiceTask oilChange = new ServiceTask(ServiceTaskName.OIL_CHANGE,
                30,100);
        Customer customer1 = new Customer("Štěpán Chudák", "Ukradená 30, 12345, Liberec",
                "chudak@nomoney.com", "733444555");
        Technician technician = new Technician("Johny Walker", "Automobilová 22, 14500, Praha 4",
                "walker@autoservissmrt.cz","723987654");
        Car ford = new Car("Ford","4AH1234",1998);
        Service service = new Service(technician,ford,customer1,oilChange);
        technician.fixTheCar(service);

        assertEquals(Boolean.TRUE, service.isFinished(),"Service should be done");
    }

    @Test
    public void testPriceIsGreater() {

        ServiceTask oilChange = new ServiceTask(ServiceTaskName.OIL_CHANGE,
                30,100);
        Customer customer1 = new Customer("Štěpán Chudák", "Ukradená 30, 12345, Liberec",
                "chudak@nomoney.com", "733444555");
        Technician technician = new Technician("Johny Walker", "Automobilová 22, 14500, Praha 4",
                "walker@autoservissmrt.cz","723987654");
        Car ford = new Car("Ford","4AH1234",1998);
        Service service = new Service(technician,ford,customer1,oilChange);

        technician.smokeCigarete(service);

        assertNotEquals(oilChange.getTypicalPriceInEuros(),service.getPrice(), "If technician smoke," +
                "the price will be greater");

    }
}





