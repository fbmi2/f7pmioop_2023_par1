package cvut.fbmi.oop.cv07ex;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class CalculatorTest {

    @Test
    public void testAdd() {
        Calculator calculator = new Calculator();
        int result = calculator.add(3,5);
        assertEquals(8, result,"3+5 should equal 8");
    }

    @Test
    public void testSubtract() {
        Calculator calculator = new Calculator();
        int result = calculator.subtract(10,4);
        assertEquals(6, result, "Result 10-4 should be 6");
    }

}
