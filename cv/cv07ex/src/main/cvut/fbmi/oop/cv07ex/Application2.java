package cvut.fbmi.oop.cv07ex;

public class Application2 {

    public static void main(String[] args) {

        FourWheeler toyota = new FourWheeler("Toyota");

        toyota.drive();
        toyota.accelerate();
        System.out.println(toyota.getBrand());

        Car subaru = new FourWheeler("Subaru");
        Car anycar = new FourWheeler("Ford");
        Car tatra = new SixWheeler("Tatra");

    }

}

interface Vehicle {
    void drive();
    String getBrand();
}

interface CarFeatures {
    void accelerate();
}

abstract class Car implements Vehicle, CarFeatures {

    private String brand;
    private int numberOfWheels;

    public Car(String brand, int numberOfWheels) {
        this.brand = brand;
        this.numberOfWheels = numberOfWheels;
    }

    @Override
    public void drive() {
        System.out.println("Driving car");
    }

    @Override
    public String getBrand() {
        return brand;
    }

    @Override
    public void accelerate() {
        System.out.println("Accelerating!");
    }
}


class FourWheeler extends Car implements CarFeatures,Vehicle {

    public FourWheeler(String brand) {
        super(brand, 4);
    }
}

class SixWheeler extends Car implements CarFeatures, Vehicle {

    public SixWheeler(String brand) {
        super(brand, 6);
    }
}


