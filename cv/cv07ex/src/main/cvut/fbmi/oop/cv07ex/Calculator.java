package cvut.fbmi.oop.cv07ex;

public class Calculator {

    public int add(int a, int b) {
        return a+b;
    }

    public int subtract(int a, int b) {
        return a-b;
    }
}
