package cvut.fbmi.oop.cv07ex;

public class Application {

    public static void main(String[] args) {
        Shape circle = new Circle(2);
        Shape squqre = new Square(6);

        System.out.println("Circle area: " + circle.calculateArea());
        circle.display();
        System.out.println("Square area: " + squqre.calculateArea());
        squqre.display();
    }

}

interface Shape {
    double calculateArea();
    void display();
}

class Circle implements Shape {

    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double calculateArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public void display() {
        System.out.println("This is a circle");
    }
}

class Square implements Shape {

    private double side;

    public Square(double side) {
        this.side = side;
    }

    @Override
    public double calculateArea() {
        return side * side;
    }

    @Override
    public void display() {
        System.out.println("This is a square");
    }
}
