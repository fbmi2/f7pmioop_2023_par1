package cvut.fbmi.oop.model;

import java.util.List;

public class User {

    private String username;
    private List<User> friends;
    private List<Post> posts;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<User> getFriends() {
        return friends;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void addFriend(User myNewFriend) {
        this.friends.add(myNewFriend);
        myNewFriend.getFriends().add(this);
    }

    public void addPost(String text) {
        Post post = new Post(this, text);
        posts.add(post);
        //TODO notify observers
    }

}
