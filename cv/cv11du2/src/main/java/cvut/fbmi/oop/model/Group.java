package cvut.fbmi.oop.model;

import java.util.ArrayList;
import java.util.List;

public class Group {

    private String name;
    private List<User> members;
    private List<Post> posts;

    public Group(String name) {
        this.name = name;
        this.members = new ArrayList<>();
        this.posts = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<User> getMembers() {
        return members;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void addMember(User user) {
        members.add(user);
    }

    public void addPost(User author, String text) {
        Post post = new Post(author, text, this);
        posts.add(post);
        //TODO notify observers
    }

}
