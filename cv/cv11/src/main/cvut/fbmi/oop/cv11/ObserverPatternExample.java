package cvut.fbmi.oop.cv11;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

// Subject interface
interface WeatherSubject {
    void addObserver(WeatherObserver observer);
    void removeObserver(WeatherObserver observer);
    void notifyObservers();
}

// Observer interface
interface WeatherObserver {
    void update(int temperature);
}

// Concrete subject
class WeatherStation implements WeatherSubject {
    private List<WeatherObserver> observers = new ArrayList<>();
    private int temperature;

    @Override
    public void addObserver(WeatherObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(WeatherObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (WeatherObserver observer : observers) {
            observer.update(temperature);
        }
    }

    // Simulate changing weather conditions
    public void setTemperature(int temperature) {
        this.temperature = temperature;
        notifyObservers();
    }
}

// Concrete observer
class WeatherDisplay implements WeatherObserver {
    private String name;

    public WeatherDisplay(String name) {
        this.name = name;
    }

    @Override
    public void update(int temperature) {
        System.out.println(name + ": Temperature is now " + temperature + " degrees Celsius");
    }
}

public class ObserverPatternExample {
    public static void main(String[] args) {
        WeatherStation weatherStation = new WeatherStation();

        // Create observers (displays)
        WeatherObserver display1 = new WeatherDisplay("Display 1");
        WeatherObserver display2 = new WeatherDisplay("Display 2");
        WeatherObserver display3 = new WeatherDisplay("Display 3");

        // Register observers with the subject (weather station)
        weatherStation.addObserver(display1);
        weatherStation.addObserver(display2);
        weatherStation.addObserver(display3);

        // Simulate changing weather conditions
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            int temperature = random.nextInt(30); // Random temperature between 0 and 30 degrees Celsius
            weatherStation.setTemperature(temperature);
            System.out.println("------------------------");
        }

        // Remove an observer
        weatherStation.removeObserver(display2);

        // Simulate more changing weather conditions
        for (int i = 0; i < 5; i++) {
            int temperature = random.nextInt(30);
            weatherStation.setTemperature(temperature);
            System.out.println("------------------------");
        }
    }
}
