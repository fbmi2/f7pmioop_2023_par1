package cz.cvut.oop.cv07.model;

public class Team {

    private String teamName;
    private String manager;

    public Team(String teamName, String manager) {
        this.teamName = teamName;
        this.manager = manager;
    }

    public String getTeamName() {
        return teamName;
    }

    public String getManager() {
        return manager;
    }
}
