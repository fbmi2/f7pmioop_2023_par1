import java.util.Scanner;

public class Cviceni3 {


    static void pozdrav(String jmeno) {
        System.out.println("Hello " + jmeno);
    }

    static void vypocetObsahuKruznice(int polomer) {
        double pi = 3.14;
        System.out.println((polomer*polomer)*pi);
    }

    static String concat(String x, String y) {
        return x + " " + y;
    }

    static int sumStrings(String x, String y) {
        return Integer.parseInt(x) + Integer.parseInt(y);
    }

    static int parseUserInput(String line) {
        int i = Integer.parseInt(line);
        if (i < 0) {
            System.out.println("NELZE SECIST ZAPORNOU HODNOTU");
        }
        return i;
    }

    static void calculator2() {
        System.out.println("VITEJTE!!!");
        Scanner scanner = new Scanner(System.in);
        System.out.print("ZADEJTE PRVNI HODNOTU SOUCTU: ");
        try {
            int a = parseUserInput(scanner.nextLine());
            //podminka CODE GUARD
            if (a < 0) {
                return;
            }
            System.out.print("ZADEJTE DRUHOU HODNOTU SOUCTU: ");
            int b = parseUserInput(scanner.nextLine());
            if (b < 0) {
                return;
            }
            System.out.println("VYSLEDEK: " + (a + b));
        } catch (NumberFormatException e) {
            System.out.println("MUZEME SCITAT POUZE CISELNE/NEZAPORNE HODNOTY");
        }
    }

    static void sumArray(int[] intArr){
        int result = intArr[0] + intArr[1] + intArr[2] + intArr[3] + intArr[4];
        System.out.println(result);
    }

    public static void main(String[] args) {


        String aaa = new String("aaa");
        double[] doubleArray = new double[5];
        String[] stringArray = new String[3];
        int[] intArray = {1,2,3,4,5,6,7};
        sumArray(intArray);
        int[] intArray2 = {1,2,3};
        sumArray(intArray2);

        int[] age = new int[5];
        Scanner[] scanners = new Scanner[3];


        //initialize array
        age[0] = 12;
        age[1] = 4;
        age[2] = 5;

        stringArray[0] = "ahoj";
        stringArray[1] = "hey";

        System.out.println("Prvni index ma hodnotu " + age[0]);
        int promenna1 = age[2];
        System.out.println(age[3]);
        System.out.println(stringArray[2]);


        //calculator2();
        String cislo = "333";
        int result = 1;
        int number = 1001;

        if (number != 0) {
            System.out.println("The number is not equal to 0");
        }

        if (number >= 1000) {
            System.out.println("The number is at least 1000");
        }

        if (result > 1) {
            System.out.println("The result is greater than 1");
        } else if (result == 0) {
            System.out.println("The result is equal 0");
        } else {
            System.out.println("The result is less or equal to 1");
        }

    }



}
