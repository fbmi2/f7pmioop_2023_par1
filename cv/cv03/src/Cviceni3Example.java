import java.util.Scanner;

public class Cviceni3Example {

    public static void main(String[] args) {
        //TODO zde se vyzkousejte od/zakomentovavat prepripravene metody a sledujte chovani
        noTryCatchExample();
        tryCatchExample();

        //calculator1();
        //calculator2();
    }

    static void calculator1() {
        System.out.println("VITEJTE!!!");
        Scanner scanner = new Scanner(System.in);
        System.out.print("ZADEJTE PRVNI HODNOTU SOUCTU: ");
        int a = scanner.nextInt();
        if (a > 0) {
            System.out.print("ZADEJTE DRUHOU HODNOTU SOUCTU: ");
            int b = scanner.nextInt();
            if (b > 0) {
                System.out.println("VYSLEDEK: " + (a + b));
            } else {
                System.out.println("NELZE SECIST ZAPORNOU HODNOTU");
            }
        } else {
            System.out.println("NELZE SECIST ZAPORNOU HODNOTU");
        }
    }

    static void calculator2() {
        System.out.println("VITEJTE!!!");
        Scanner scanner = new Scanner(System.in);
        System.out.print("ZADEJTE PRVNI HODNOTU SOUCTU: ");
        try {
            int a = parseUserInput(scanner.nextLine());
            //podminka CODE GUARD
            if (a < 0) {
                return;
            }
            System.out.print("ZADEJTE DRUHOU HODNOTU SOUCTU: ");
            int b = parseUserInput(scanner.nextLine());
            if (b < 0) {
                return;
            }
            System.out.println("VYSLEDEK: " + (a + b));
        } catch (NumberFormatException e) {
            System.out.println("MUZEME SCITAT POUZE CISELNE/NEZAPORNE HODNOTY");
        }
    }

    static int parseUserInput(String line) {
        int i = Integer.parseInt(line);
        if (i < 0) {
            System.out.println("NELZE SECIST ZAPORNOU HODNOTU");
        }
        return i;
    }

    //zde jsme zavolali metodu throwError, ktera vyhodi vyjimku, kterou ale odchyti specifikovany catch-blok
    //vykona se tedy nami specifikovany kod s vypisem hlasky, vyjimka se zde tzv. zatopi, tj. NEvyhazujeme ji dal
    // s pomoci throw klauzule
    static void tryCatchExample() {
        try {
            throwError();
        } catch (NumberFormatException e) {
            System.out.println("NASTALA CHYBA");
            //throw e
        }
    }

    //zde se vola metoda throwError, jejíz exekuce vyhodi chybu, jelikoz zde neni zadny try-catch blok
    //chyba je ciste vyhozena a konci i exekuce noTryCatchExample metody -> chyba se posunuje dale ke
    //zdroji volaní, v nasem pripade metoda main, pokud ani tam neni odchycena, tak je vypsana do konzole
    //a konci i exekuce cele aplikace
    static void noTryCatchExample() {
        throwError();
    }

    //takto je mozne z metody vyhodit chyby/vyjimku
    //vyhozeni vyjimky prerusuje code-flow, takze po vyhozeni vykonavani metody konci
    static int throwError() {
        throw new NumberFormatException("CHYBA 1");
    }

    static void conditions() {
        System.out.println(concat("A", "B"));
        int result = sumStrings("1", "2");

        // && AND
        // || OR
        // !=
        // ==
        // !
        // kalkulačka bude umět sčítat jen kladná čísla, v případě zadání záporného vstupu se vytiskne hláška
        // a aplikace nic neudělá (nesečte) - skončí
        if (result == 0 && true) {
            System.out.println("CISLO JE VETSI NEZ 0");
        }
        if (false) {

        }

        if (result > 1) {
            //
        } else if (result == 0) {

        } else {

        }
    }

    static String concat(String x, String y) {
        return x + " " + y;
    }

    static int sumStrings(String x, String y) {
        return Integer.parseInt(x) + Integer.parseInt(y);
    }
}
