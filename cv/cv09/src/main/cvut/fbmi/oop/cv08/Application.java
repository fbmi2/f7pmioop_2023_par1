package cvut.fbmi.oop.cv08;

import cvut.fbmi.oop.cv08.model.User;

public class Application {

    public static void main(String[] args) {

        MyLogger logger = MyLogger.getInstance();

        logger.log("Application started");
        logger.log("Processing data");
        logger.log("Application finnished");

        MyLogger logger2 = MyLogger.getInstance();
        System.out.println(logger == logger2);

        String myString = new String("ABC");

    }

}
