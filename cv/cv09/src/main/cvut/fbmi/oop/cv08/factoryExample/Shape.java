package cvut.fbmi.oop.cv08.factoryExample;

// Step 1: Product (Shape interface)

public interface Shape {
    void draw();
}
