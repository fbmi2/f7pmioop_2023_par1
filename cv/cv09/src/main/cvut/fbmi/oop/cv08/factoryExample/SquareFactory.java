package cvut.fbmi.oop.cv08.factoryExample;

public class SquareFactory implements ShapeFactory{
    @Override
    public Shape createShape() {
        return new Square();
    }
}
