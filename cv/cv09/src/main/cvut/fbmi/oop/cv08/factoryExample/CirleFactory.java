package cvut.fbmi.oop.cv08.factoryExample;

public class CirleFactory implements ShapeFactory{
    @Override
    public Shape createShape() {
        return new Circle();
    }
}
