package cvut.fbmi.oop.cv08.model;

public class User {


    private static int MAX_USER_COUNT = 1000000;
    // Static variable
    public static int userCount = 0;

    // Final variable (constant)
    private final String userId;

    // Instance variables
    private String username;
    private String email;

    // Static method to get the user count
    public static int getUserCount() {
        return userCount;
    }

    // Constructor with final variable initialization
    public User(String username, String email) {
        this.username = username;
        this.email = email;

        // Increment user count in the constructor
        userCount++;

        // Assign a unique user ID based on the count
        this.userId = "U" + userCount;

        // Print a message indicating the creation of a new user
        System.out.println("New user created: " + username + ", User ID: " + userId);
    }

    // Instance method to get the user ID
    public String getUserId() {
        return userId;
    }

    // Static method demonstrating a utility function
    public static void printUserCount() {
        System.out.println("Total number of users: " + getUserCount());
    }

    // Final method that cannot be overridden
    public final void displayUserInfo() {
        System.out.println("User ID: " + userId);
        System.out.println("Username: " + username);
        System.out.println("Email: " + email);
    }
}

