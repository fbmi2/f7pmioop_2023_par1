package cvut.fbmi.oop.cv08;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class MyLogger {

    private static MyLogger instance;

    private String logFile = "application.log";

    private MyLogger() {
    }

    public static MyLogger getInstance() {
        if (instance == null) {
            instance = new MyLogger();
        }
        return instance;
    }

    public void log(String message) {
        try (PrintWriter writer = new PrintWriter(new FileWriter(logFile, true))) {
            writer.println(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
