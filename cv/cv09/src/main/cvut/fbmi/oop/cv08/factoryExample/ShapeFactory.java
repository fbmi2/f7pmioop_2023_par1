package cvut.fbmi.oop.cv08.factoryExample;

// Step 3: Factory (ShapeFactory interface)
public interface ShapeFactory {
    Shape createShape();
}
