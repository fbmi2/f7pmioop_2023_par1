package cvut.fbmi.oop.cv08.factoryExample;

public class RectangleFactory implements ShapeFactory{
    @Override
    public Shape createShape() {
        return new Rectangle();
    }
}
