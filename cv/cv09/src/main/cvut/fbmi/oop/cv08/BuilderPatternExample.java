package cvut.fbmi.oop.cv08;

import cvut.fbmi.oop.cv08.model.Computer;

public class BuilderPatternExample {
    public static void main(String[] args) {
        // Using the builder to create a Computer object
        Computer computer = Computer.ComputerBuilder.builder()
                .processor("Intel i7")
                .memory(16)
                .storage(512)
                .graphicsCard("NVIDIA GeForce RTX 3070")
                .build();

        Computer computer2 = Computer.ComputerBuilder.builder()
                .graphicsCard("AMD RX 5600")
                .memory(32)
                .processor("AMD EPYC Z2")
                .storage(1000)
                .build();

        // Displaying the created Computer object
        System.out.println("Processor: " + computer.getProcessor());
        System.out.println("Memory: " + computer.getMemoryGB() + " GB");
        System.out.println("Storage: " + computer.getStorageGB() + " GB");
        System.out.println("Graphics Card: " + computer.getGraphicCard());
    }
}
