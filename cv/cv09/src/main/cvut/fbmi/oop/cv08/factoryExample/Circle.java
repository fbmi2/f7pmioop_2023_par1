package cvut.fbmi.oop.cv08.factoryExample;

public class Circle implements Shape{
    @Override
    public void draw() {
        System.out.println("Drawing a Circle");
    }
}
