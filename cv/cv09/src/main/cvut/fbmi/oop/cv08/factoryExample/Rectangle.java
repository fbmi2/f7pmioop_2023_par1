package cvut.fbmi.oop.cv08.factoryExample;

// Step 2: ConcreteProducts (Circle, Square, Rectangle)
public class Rectangle implements Shape{
    @Override
    public void draw() {
        System.out.println("Drawing a Rectangle");
    }
}
