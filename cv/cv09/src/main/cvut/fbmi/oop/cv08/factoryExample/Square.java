package cvut.fbmi.oop.cv08.factoryExample;

public class Square implements Shape{
    @Override
    public void draw() {
        System.out.println("Drawing a Square");
    }
}
