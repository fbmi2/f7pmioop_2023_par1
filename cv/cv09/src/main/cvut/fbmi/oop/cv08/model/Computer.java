package cvut.fbmi.oop.cv08.model;

public class Computer {

    private String processor;
    private int memoryGB;
    private int storageGB;
    private String graphicsCard;

    public String getProcessor() {
        return processor;
    }

    public int getMemoryGB() {
        return memoryGB;
    }

    public int getStorageGB() {
        return storageGB;
    }

    public String getGraphicCard() {
        return graphicsCard;
    }

    public static class ComputerBuilder {
        private Computer computer;

        public ComputerBuilder() {
            this.computer = new Computer();
        }

        // Methods to set the optional components
        public ComputerBuilder processor(String processor) {
            computer.processor = processor;
            return this;
        }

        public ComputerBuilder memory(int memoryGB) {
            computer.memoryGB = memoryGB;
            return this;
        }

        public ComputerBuilder storage(int storageGB) {
            computer.storageGB = storageGB;
            return this;
        }

        public ComputerBuilder graphicsCard(String graphicsCard) {
            computer.graphicsCard = graphicsCard;
            return this;
        }

        public static ComputerBuilder builder() {
            return new ComputerBuilder();
        }

        // Method to build the final product
        public Computer build() {
            return computer;
        }

    }


}
