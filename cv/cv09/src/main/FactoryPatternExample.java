import cvut.fbmi.oop.cv08.factoryExample.CirleFactory;
import cvut.fbmi.oop.cv08.factoryExample.RectangleFactory;
import cvut.fbmi.oop.cv08.factoryExample.Shape;
import cvut.fbmi.oop.cv08.factoryExample.ShapeFactory;

public class FactoryPatternExample {

    // 1. Product -> interface nebo abstraktní třída, který nesou metody, které budou implementovány konkrétním produktem
    // 2. Konkrétní produkt -> což jsou trídy, které implementují produkt nebo extendují abstraktní třídu a reprezentují objekty, které bude vyrábět továrna
    // 3. Továrna -> interface nebo abstraktní třída, která definuje metody pro vytváření produktů, ale konkrétní implementace je nechána konkrétním továrnám
    // 4. Konkrétní továrna - třídy co implementují Továrnu a jsou zodpovědné za vytváření konkrétních objektů

    public static void main(String[] args) {

        ShapeFactory circleFactory = new CirleFactory();
        Shape circle = circleFactory.createShape();
        circle.draw();
        Shape circle2 = circleFactory.createShape();
        circle2.draw();

        ShapeFactory rectangleFactory = new RectangleFactory();
        Shape rectangle = rectangleFactory.createShape();
        rectangle.draw();

    }

}
