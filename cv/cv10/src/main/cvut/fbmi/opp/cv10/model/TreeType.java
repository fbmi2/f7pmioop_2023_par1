package cvut.fbmi.opp.cv10.model;

public class TreeType implements Tree{
    private String name;
    private String color;

    public TreeType(String name, String color) {
        this.name = name;
        this.color = color;
    }

    @Override
    public void display(int x, int y) {
        System.out.println("Drawing a " + color + " " + name + " tree at position (" + x + ", " + y + ")");
    }
}
