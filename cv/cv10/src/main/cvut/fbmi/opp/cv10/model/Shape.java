package cvut.fbmi.opp.cv10.model;

public interface Shape {
    void draw();
}
