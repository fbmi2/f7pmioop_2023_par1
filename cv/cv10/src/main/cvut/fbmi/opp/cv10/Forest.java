package cvut.fbmi.opp.cv10;

import cvut.fbmi.opp.cv10.model.Tree;
import cvut.fbmi.opp.cv10.model.TreeImpl;
import cvut.fbmi.opp.cv10.model.TreeType;
import cvut.fbmi.opp.cv10.model.TreeTypeFactory;

public class Forest {

    public static void main(String[] args) {

        plantTree("Oak", "Green", 1,1);
        plantTree("Pine", "Brown", 1,2);
        plantTree("Maple", "Red", 1,3);
        plantTree("Oak", "Green", 1,4);
        plantTree("Oak", "Green", 1,5);
        plantTree("Oak", "Green", 1,6);

    }

    private static void plantTree(String name, String color, int x, int y) {
        // Get a tree type from the factory
        TreeType treeType = TreeTypeFactory.getTreeType(name, color);
        // Create a tree instance with extrinsic properties
        Tree tree = new TreeImpl(treeType, x, y);

        tree.display(x, y);
    }

}
