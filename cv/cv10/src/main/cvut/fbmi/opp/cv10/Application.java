package cvut.fbmi.opp.cv10;

import cvut.fbmi.opp.cv10.model.Circle;
import cvut.fbmi.opp.cv10.model.ShapeFactory;

public class Application {
    public static void main(String[] args) {

        Circle circle1 = (Circle) ShapeFactory.getCircle("red");
        Circle circle2 = (Circle) ShapeFactory.getCircle("blue");
        Circle circle3 = (Circle) ShapeFactory.getCircle("yellow");
        Circle circle4 = (Circle) ShapeFactory.getCircle("red");
        Circle circle5 = (Circle) ShapeFactory.getCircle("red");

        System.out.println(circle1 == circle2);
        System.out.println(circle1 == circle5);

        Circle circle6 = new Circle("red");

        System.out.println(circle1 == circle6);

    }
}
