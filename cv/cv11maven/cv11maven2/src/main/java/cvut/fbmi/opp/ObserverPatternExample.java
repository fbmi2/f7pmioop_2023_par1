package cvut.fbmi.opp;


import cvut.fbmi.opp.model.WeatherDisplay;
import cvut.fbmi.opp.model.WeatherStation;

import java.util.Random;

public class ObserverPatternExample {

    public static void main(String[] args) {

        //create subject
        WeatherStation station1 = new WeatherStation();
        station1.setTemperature(10);

        //create observers
        WeatherDisplay display1 = new WeatherDisplay("FirstOne");
        WeatherDisplay display2 = new WeatherDisplay("SecondOne");
        WeatherDisplay display3 = new WeatherDisplay("ThirdOne");

        //register observers to subject
        station1.addObserver(display1);
        station1.addObserver(display2);
        station1.addObserver(display3);

        //simulate temperature
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            int termperature = random.nextInt(30);
            station1.setTemperature(termperature);
            System.out.println("------------------------");
        }

        //remove observer from subscription
        station1.removeObserver(display1);

        for (int i = 0; i < 5; i++) {
            int termperature = random.nextInt(30);
            station1.setTemperature(termperature);
            System.out.println("------------------------");
        }

    }

}
