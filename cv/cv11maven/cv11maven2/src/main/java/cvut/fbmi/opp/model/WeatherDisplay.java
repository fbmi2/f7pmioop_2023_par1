package cvut.fbmi.opp.model;

public class WeatherDisplay implements WeatherObserver{

    private String name;

    public WeatherDisplay(String name) {
        this.name = name;
    }

    @Override
    public void update(int temperature) {
        System.out.println(name + ": Temperature is now " + temperature + " degrees Celcius");
    }
}
