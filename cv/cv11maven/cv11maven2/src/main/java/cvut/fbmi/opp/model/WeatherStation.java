package cvut.fbmi.opp.model;

import java.util.ArrayList;
import java.util.List;

public class WeatherStation implements WeatherSubject{
    private List<WeatherObserver> observers = new ArrayList<>();
    private int temperature = 5;

    @Override
    public void addObserver(WeatherObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(WeatherObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (WeatherObserver observer : observers) {
            observer.update(temperature);
        }
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
        notifyObservers();
    }
}
