package cvut.fbmi.opp.model;

public interface WeatherObserver {
    void update(int temperature);
}
